import sys
sys.path.append("../")
from hyperdex_store import *
from ConfigParser import SafeConfigParser
config_parser = SafeConfigParser()
config_parser.read('server.cfg')

node_set = set()
edge_id = 0

#host = config_parser.get('paper-repo', 'host') 
#port = config_parser.getint('paper-repo', 'port')
#
#a = hyperdex.admin.Admin(host, port)
#c = hyperdex.client.Client(host, port)
#cleanup_space(a)
#setup_space(a)

def add_node(word):
    if word not in node_set:
        node_set.add(word)
        sys.stdout.write('<node id="{0}"></node>\n'.format(word))

def add_edge(from_word, to_word, confidence, doc_id):
    global edge_id
    sys.stdout.write(('<edge id="{0}" source="{1}" target="{2}">' + \
                    '<data key="doc_id">{3}</data>' + \
                    '<data key="confidence">{4}</data></edge>\n') \
        .format(edge_id, from_word, to_word,
                doc_id, confidence))
    edge_id += 1

for line in sys.stdin.readlines():
    tokens = [i.strip() for i in line.split()]
    tokens.append("$")
    author_from = tokens[0]
    author_to = tokens[1]
    doc_id = tokens[2]
    add_node(author_from)
    add_node(author_to)
    add_edge(author_from, author_to, doc_id, 0.1)
