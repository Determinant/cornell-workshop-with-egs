import json
from exc import *
from flask import request
from hyperdex_store import get_doc
import weaver.client as wclient
import hyperdex

def _paper_repo_ajax(self):
    words = request.form['doc_id']
    try:
        res = map(lambda doc_id: [doc_id, get_doc(doc_id, self.paper_repo)],
                    self.weaver.n_gram_path(words))
    except wclient.WeaverError as e:
        lst = []
    else:
        for [doc_id, doc] in res:
            if doc is None:
                raise ServerInternalError("paper metadata not found")
        lst = [{'id':doc_id,
               'title':doc.title,
               'date':doc.date,
               'authors':doc.authors} \
                   for [doc_id, doc] in res]
    print('done')
    return json.dumps(lst)
def get_page_module(pm_cls):
    class Module(pm_cls):
        _ajax = _paper_repo_ajax
    return Module
