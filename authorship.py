import json
from flask import request
import weaver.client as wclient
def merge_subgraph(graph1, graph2):
    merged = {node: {edge.handle : edge for edge in graph1[node]} for node in graph1}
    for node in graph2:
        if not node in merged:
            merged[node] = {}
        medges = merged[node]
        for edge in graph2[node]:
            if not edge.handle in medges:
                medges[edge.handle] = edge
    return {node: [e for h, e in merged[node].iteritems()] for node in merged}

def _authorship_ajax(self):
    author_from = request.form['from'].encode("utf-8")
    author_to = request.form['to'].encode("utf-8")
    try:
        author_step = int(request.form['step'])
    except ValueError:
        author_step = self._max_step
    author_step = min(self._max_step, author_step)
    try:
        res, path = self.weaver.discover_paths(author_from, author_to,
                                                path_len=author_step)
    except wclient.WeaverError:
        res = {}
#    d = pm._max_step
#    for node in path:
#        res = merge_subgraph(author_weaver.discover_paths(
#            node, '', path_len = max(d, pm._max_step - d))[0], res)
#        d -= 1
    lst = {author: [{'to': e.end_node, 'doc_id': e.properties["doc_id"][0]} \
            for e in res[author]] for author in res}
    return json.dumps(lst)

def get_page_module(pm_cls):
    class Module(pm_cls):
        _ajax = _authorship_ajax
        def __init__(self, *args):
            super(Module, self).__init__(*args)
            self._max_step = self.config_parser.getint('authorship', 'max_step') or 4
    return Module
