import json
from exc import *
from flask import request
from hyperdex_store import get_doc
from collections import OrderedDict
from time import sleep # for debug
import utils
import weaver.client as wclient
import hyperdex.client as hclient

MAX_CACHED_RESULTS = 5
MAX_ENTRY_NUM = 10

def _get_histogram(self, words):
    try:
        res = map(lambda doc_id: get_doc(doc_id, self.paper_repo),
                    self.weaver.n_gram_path(words))
    except wclient.WeaverError as e:
        res = []
    except hclient.HyperDexClientException as e:
        raise ServerInternalError("paper store is not setup")
    else:
        for doc in res:
            if doc is None:
                raise ServerInternalError("paper metadata not found")
    return utils.date_histogram(res, utils.MIN_DATE, utils.MAX_DATE, utils.BIN_NUM)
def _get_histogram_cached(self, words):
    cache_key = '_'.join(words)
    try:
        detailed_res = self._cache.pop(cache_key)
        self._cache[cache_key] = detailed_res
        print("cache hit")
    except KeyError:
        print("cache miss")
        try:
            self._cache.pop(cache_key)
        except KeyError:
            if len(self._cache) >= MAX_CACHED_RESULTS:
                self._cache.popitem(last=False)
        detailed_res = self._get_histogram(words)
        self._cache[cache_key] = detailed_res
    return detailed_res

def _histogram_handler(self):
    words = [word.encode("utf-8") for word in json.loads(request.form['words'])]
    print('request: {0}'.format(str(words)))
    detailed_res = _get_histogram_cached(self, words)
    sleep(2)
    return {'start_date': utils.MIN_DATE,
            'end_date': utils.MAX_DATE,
            'bin_num': utils.BIN_NUM,
            'hist' :[len(i) for i in detailed_res]}

def _histogram_bin_handler(self):
    words = [word.encode("utf-8") for word in json.loads(request.form['words'])]
    detailed_res = _get_histogram_cached(self, words)
    idx = int(request.form['idx'])
    entry_start = int(request.form['entry_start'])
    entry_num = int(request.form['entry_num'])
    if entry_num > MAX_ENTRY_NUM:
        print("too many entries requested")
        return ""
    sleep(4)
    return [{'id':doc.id,
           'title':doc.title,
           'date':doc.date,
           'authors':doc.authors} \
                   for doc in detailed_res[idx][entry_start:entry_start + entry_num]]

_n_gram_ajax_handler = {
        'hist': _histogram_handler,
        'bin': _histogram_bin_handler
}

def _n_gram_ajax(self):
    act = request.form['action']
    try:
        res = _n_gram_ajax_handler[act](self)
        return json.dumps(res)
    except KeyError:
        print("invalid action: {0}".format(act))
        return ""
def get_page_module(pm_cls):
    class Module(pm_cls):
        _get_histogram = _get_histogram
        _ajax = _n_gram_ajax
        def __init__(self, *args):
            super(Module, self).__init__(*args)
            self._cache = OrderedDict()
    return Module
