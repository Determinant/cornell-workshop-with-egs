How To Run the Web Server
=========================

- Get Weaver from https://github.com/Determinant/weaver.git and compile.
- Configure and start the Weaver instances properly.
- Modify the configuration file ``server.cfg``, change the host IP and port to
  your instances, and remove the names of unwanted modules from ``[general]``.
  For example, to only test n-gram, just leave ``modules: n-gram, about``,
  ``about`` is for the about page (to be written).
- Start the server by ``python server.py``.
